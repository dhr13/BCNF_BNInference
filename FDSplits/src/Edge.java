
public class Edge<Vertex, Datatype> {
	Vertex v1;
	Vertex v2;
	Datatype data;
	
	public Edge(Vertex v1, Vertex v2, Datatype data){
		this.v1 = v1;
		this.v2 = v2;
		this.data = data;
	}
	public Edge(Vertex v1, Vertex v2){
		this(v1, v2, null);
	}
	
	@Override
	public boolean equals(Object o){
		if(! (o  instanceof Edge<?, ?>)){
			return false;
		}
		Edge otherEdge = (Edge)o;
		return otherEdge.v1.equals(this.v1) && otherEdge.v2.equals(this.v2);
	}
	
	@Override
	public int hashCode(){
		return this.v1.hashCode() + this.v2.hashCode();
	}
	
	public void reverse(){
		Vertex holder = this.v1;
		this.v1 = this.v2;
		this.v2 = holder;
	}
}
