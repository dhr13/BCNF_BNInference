import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;


/**
 * This is a bayesian network where all verticies are
 * integers, with a fixed number of possible values.
 * 
 * Each vertex has an associated discrete Conditional Probability (CP) table,
 *  and an associated set of parents.
 *  
 *  This class is mainly used for data generation rather than learning, and includes the ability
 *  to generate a fully functioning bayesian network from a skeleton (passed as an integer network),
 *  and the ability to produce a sample by using the feedForward method.
 * 
 * 
 * @author dannyrivers
 *
 */
public class DiscreteBayesianNetwork{
	ArrayList<Integer> topologicalSort;
	
	ArrayList<ArrayList<Integer>> parentSets;
	
	DiscreteCPTable[] cpts;
	
	int numValsForEachVariable;
	
	
	/**
	 * Make a new Discrete Bayesian Network by randomly parameterizing this
	 * network.
	 * @param input
	 */
	public DiscreteBayesianNetwork(Network<Integer, Integer> input, int numValuesForEachVariable){
		topologicalSort = input.topologicalSort();
		parentSets = input.parentSets();
		numValsForEachVariable = numValuesForEachVariable;
		
		cpts = new DiscreteCPTable[input.verticies.size()];
		for(int i = 0; i < cpts.length ; i ++){
			cpts[i] = DiscreteCPTable.makeRandomTable(numValuesForEachVariable);
		}
	}
	
	
	/**
	 * Produce assignemnts for all variables in the network according to the
	 * feed forward algorithm, which follows a topological sort through the network,
	 * assigning variables according to the conditional distributions enforced by
	 * this particular setting of the parents.
	 * @return
	 */
	public Integer[] feedForward(){
		return feedForward(new HashMap<Integer, Integer>(0));
	}
	/**
	 * Produce random values for all the variables in the network, 
	 * given the fixed values provided in context. There is no backward
	 * information flow, so the variables in context are treated as interventions,
	 * NOT as being conditioned on.
	 * @param context
	 * @return
	 */
	public Integer[] feedForward(HashMap<Integer, Integer> context){
		
		//setup
		Integer[] result = new Integer[topologicalSort.size()];
		boolean[] processed = new boolean[topologicalSort.size()];
		for(int i = 0; i < processed.length ; i ++){
			processed[i] = false;
		}
		for(Integer k : context.keySet()){
			result[k] = context.get(k);
			processed[k] = true;
		}
		
		// process each variable according to the topological sort
		for(int i = 0; i < cpts.length ; i ++){
			if(!processed[i]){
				int varIndex = topologicalSort.get(i);
				ArrayList<Integer> parents = parentSets.get(varIndex);
				Integer[] parentValues = new Integer[parents.size()];
				for(int j = 0; j < parents.size(); j ++){
					int parentIndex = parents.get(j);
					parentValues[j] = result[parentIndex];
				}
				result[varIndex] = cpts[varIndex].sample(parentValues);
			}
		}
		return result;
	}
	
	
	
	//////////////////////
	//////////////////////
	////////  Testing
	//////////////////////
	//////////////////////
	
	
	
	public static void main(String[] args){
			Network<Integer, Integer> testNetwork = Network.testNetwork();
			
			DiscreteBayesianNetwork d = new DiscreteBayesianNetwork(testNetwork, 3);
			testFeedForward(d, 1000);
	}
	
	public static void testFeedForward(DiscreteBayesianNetwork d, int numTests){
		int[][] histograms = new int[d.topologicalSort.size()][d.numValsForEachVariable];
		
		HashMap<Integer, Integer> settings = new HashMap<Integer, Integer>();
		settings.put(3, 2);
		for(int i = 0; i < numTests ; i ++){
			Integer[] result = d.feedForward(settings);
			for(int j = 0; j< result.length ; j ++){
				histograms[j][result[j]]++;
			}
		}
		System.out.println("Testing feed forward");
		System.out.println("Topological sort of the network: ");
		System.out.println(d.topologicalSort);
		System.out.println("Each entry will be a histogram for one variable, showing the number");
		System.out.println(" of times that the variable attained each value after numTests runs of the network");
		for(int[] histogram : histograms){
			System.out.println(Arrays.toString(histogram));
		}
	}
	
}
