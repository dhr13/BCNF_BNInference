
/**
 * A class that determines which attributes functionally 
 * determine which other attributes. Represents the functional dependence
 * 	lhs -> rhs
 * 
 * When used to describe a BCNF decomposition, the lhs will be attribute
 *   indices in the "larger" relation (the relation that completely 
 *   functionally determines the other relation), and the rhs will be attribute
 *   indices in the "smaller" relation.
 * 
 * @author dannyrivers
 *
 */
public class FunctionalDependence {
	int[] lhs;
	int[] rhs;
}
