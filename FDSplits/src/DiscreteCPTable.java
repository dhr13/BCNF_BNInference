import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Random;
import java.util.function.Function;

/**
 * A conditional probability table for variables with discrete domains.
 * 
 * This class represents the probabilities for a single variable.
 * 
 * Assumes discrete input and discrete output.
 * 
 * Using the sample(parents) method will pick a value for that variable
 *   according to the distribution of that variable conditional on the 
 *   values of parents.
 * 
 * 
 * @author dannyrivers
 *
 */
public class DiscreteCPTable implements CPTable<Integer>{
	Function<Integer[], Double[]> pf;
	static Random r = new Random();
	
	public DiscreteCPTable(Function<Integer[], Double[]> probabilitiesFunction){
		this.pf = probabilitiesFunction;
	}
	
	/**
	 * Make a random CPTable as needed.
	 * This function will return a DiscreteCPTable that
	 * stores the previously seen 
	 * @param numValues
	 * @return
	 */
	public static DiscreteCPTable makeRandomTable(int numValues){
		//make a lookup function.
		// The function that returns probabilities will either
		// return the stored probability function (the Double[]) 
		// if this parent set has been seen, or else 
		// create a new probability function and store it if this
		// parent set hasn't been seen.
		HashMap<ArrayList<Integer>, Double[]> lookup = new HashMap<ArrayList<Integer>, Double[]>();

		Random r2 = new Random(r.nextInt());
		
		Function<Integer[], Double[]> f = parents -> 
		{
			ArrayList<Integer> parentsList = new ArrayList<Integer>();
			for(Integer i : parents){
				parentsList.add(i);
			}
			Double[] result = lookup.get(parentsList);
			if(result != null)return result;
			else{
				result = makeNewProbabilityFunction(numValues, r2);
				lookup.put(parentsList, result);
				return result;
			}
		};
		return new DiscreteCPTable(f);
	}
	
	/**
	 * Return a random probability distribution over numValues values.
	 * 
	 * @param numValues
	 * @param r2
	 * @return
	 */
	public static Double[] makeNewProbabilityFunction(int numValues, Random r2){
		int sum = 0;
		double[] values = new double[numValues];
		
		for(int i = 0; i < numValues ; i ++){
			int nextInt = r.nextInt(1 << 8); //make probabilities with 8 bits of precision.
			values[i] = nextInt;
			sum += nextInt;
		}
		Double[] result = new Double[numValues];
		for(int i = 0; i < numValues;  i ++){
			result[i] = values[i] / sum;
		}
		return result;
	}
	
	/**
	 * Given these parents, decide on a value for this variable at random.
	 */
	public Integer sample(Integer[] parents){
		int result = 0;
		double runningTotal = 0;
		Double[] parts = pf.apply(parents);
		double rVal = r.nextDouble();
		while(runningTotal < rVal && result < parts.length){
			runningTotal += parts[result];
			result ++;
		}
		return result - 1;
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	//////////////////////
	//////////////////////
	////////  Testing
	//////////////////////
	//////////////////////
	
	
	
	
	
	
	
	
	
	
	
	
	public static void main(String[] args){
		
		testRandomDistributionGeneration(100);
		//testRandomGeneration(5, 1000, 10);
		
		//testKnownCPTable(10000);
	}
	
	public static void testRandomGeneration(int numValues, int numTests, int numRepeats ){
		System.out.println("testing random generation of a discrete conditional probability table.");
		System.out.println(" Should show a histogram of numValues elements, and each element should be even every time.");
		System.out.println(" Aside from being a multiple of numRepeats and adding up to numTests, the values in the histogram should be random.");
		DiscreteCPTable d = makeRandomTable(numValues);
		int[] histogram = new int[numValues];
		for(int j = 0; j < numRepeats ; j ++){
			for(int i = 0; i < numTests / numRepeats ; i ++){
				histogram[d.sample(new Integer[]{i/5, i % 5})] ++;
			}
		}
		System.out.println(Arrays.toString(histogram));
		
	}
	
	public static void testRandomDistributionGeneration(int numTests){
		
		int histogramFidelity = 5;
		int[] histogram = new int[histogramFidelity];
		
		int numOutputs = 5;
		
		System.out.println("Testing generation of probability distributions");
		for(int i = 0; i < numTests ; i ++){
			Double[] newDist=  makeNewProbabilityFunction(numOutputs, r);
			for(Double d : newDist){
				histogram[(int)(d * histogramFidelity)] ++;
			}
		}
		System.out.println(Arrays.toString(histogram));
	}
	
	
	public static void testKnownCPTable( int numTests){
		double low = 0.1;
		double high = 0.9;
		//Unit test for a discrete probability table.
		Function<Integer[], Double[]> f = parents -> (parents[0] >= 1)? new Double[]{0.5, 0.5}:new Double[]{low, high} ;
		DiscreteCPTable p = new DiscreteCPTable(f);
		int[] histogram = new int[2];
		for(int i = 0; i < numTests ; i ++){
			if(i % 2 == 1){
				histogram[p.sample(new Integer[]{1})] ++;
			}
			else{
				histogram[p.sample(new Integer[]{0})] ++;
			}
		}
		System.out.println(Arrays.toString(histogram));
		System.out.println("Should be " + (0.5 * (0.5 + low) * numTests) + "," + (0.5 * (0.5 + high) * numTests));
	}
	
}
