import java.util.HashMap;
import java.util.HashSet;


public class Relation {
	
	int numAttributes;

	HashSet<Integer> attributes;
	HashSet<Integer> primaryKey; //the attribute numbers that form the primary key for this relation.
	// Primary keys will be removed in the final bayesian network.
	
	String name; //must match the name in the database.
	int relationNumber;
	

	HashMap<Integer, String> attributeNames;
	
	int maxNumRelations = 1 << 8; //Each attribute will have a unique identifier 
	// equal to (attributeNumber*maxNumRelations + relationNumber).
	
	
	/**
	 * 
	 * @param relationNum
	 * @param numAttributes
	 * @param primaryKey - the primary keys, according to local attribute numbers (not global)
	 */
	public Relation(int relationNum, int numAttributes, int[] primaryKey){
		this.numAttributes = numAttributes;
		this.primaryKey = new HashSet<Integer>();
		for(int i : primaryKey){
			this.primaryKey.add(attributeKeyFor(i));
		}
		this.attributes = new HashSet<Integer>();
		for(int i = 0; i < numAttributes ; i ++){
			this.attributes.add(i);
		}
	}
	
	
	/**
	 *  Each attribute will have a unique identifier   
	 *  equal to (attributeNumber*maxNumRelations + relationNumber).
	 * @param r
	 * @param attributeNum
	 * @return
	 */
	public Integer attributeKeyFor(int attributeNum){
		int relationNumber = getRelationNumber();
		return attributeNum*maxNumRelations + relationNumber;
	}
	
	public int getRelationNumber(){
		return this.relationNumber;
	}
	
}
