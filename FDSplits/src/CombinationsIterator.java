import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.Iterator;

/**
 * A class that iterates over all combinations of the given set.
 * 
 * Returns ArrayLists after each iteration. 
 * @author dannyrivers
 *
 * @param <T>
 */
public class CombinationsIterator<T> implements Iterable<ArrayList<T>>, Iterator<ArrayList<T>>{
	
	private ArrayList<T> items;
	private int num;
	public CombinationsIterator(Iterable<T> set){
		if(set instanceof Collection){
			this.items = new ArrayList<T>(((Collection) set).size());
		}
		else{
			this.items = new ArrayList<T>();
		}
		for(T item : set){
			this.items.add(item);
		}
		this.num = 0;
	}
	@Override
	public Iterator<ArrayList<T>> iterator() {
		return this;
	}
	@Override
	public boolean hasNext() {
		return num < (1 << items.size());
	}
	@Override
	public ArrayList<T> next() {
		ArrayList<T> result = new ArrayList<T>(items.size() / 2); 
		int mask = num;
		for(int i = 0; i < items.size() ; i ++){
			if((mask & 1) == 1){
				result.add(items.get(i));
			}
		}
		return result;
	}
	
	
}
