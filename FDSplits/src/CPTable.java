
/**
 * A conditional probability table for a single variable.
 * 
 * Creates a value for a variable based on the values of the parents of that variable.
 * 
 * @author dannyrivers
 *
 * @param <T>
 */
public interface CPTable<T> {
	/**
	 * Pick a value for this variable 
	 * @param parents
	 * @return
	 */
	public T sample(T[] parents);
}
