import java.util.ArrayList;
import java.util.HashSet;


public class Network<Vertex, EdgeDataType> {
	ArrayList<Vertex> verticies;
	HashSet<Edge<Vertex, EdgeDataType>> edges;
	
	public Network(){
		verticies = new ArrayList<Vertex>();
		edges = new HashSet<Edge<Vertex, EdgeDataType>>();
	}
	
	
	public Network( Vertex[] verticies, int[][] edges){
		this.verticies = new ArrayList<Vertex>(verticies.length);
		this.edges = new HashSet<Edge<Vertex, EdgeDataType>>(edges.length);
		for(int i = 0; i < verticies.length ; i ++){
			addVertex(verticies[i]);
		}
		for(int[] edge : edges){
			addEdge(edge[0], edge[1]);
		}
	}
	
	public void addVertex(Vertex v){
		verticies.add(v);
	}
	
	/**
	 * Adds the edge if it doesn't exist.
	 * @param e
	 */
	public void addEdge(Edge<Vertex, EdgeDataType> e){
		edges.add(e);
	}
	
	public void addEdge(int v1, int v2){
		edges.add(new Edge<Vertex, EdgeDataType>(verticies.get(v1), verticies.get(v2)));
	}
	
	public void removeEdge(Edge<Vertex, EdgeDataType> e){
		edges.remove(e);
	}
	
	
	public void reverseEdges(){
		for(Edge e : edges){
			e.reverse();
		}
	}
	
	public HashSet<Vertex> neighbors(HashSet<Vertex> k){
		HashSet<Vertex> result = new HashSet<Vertex>();
		for(Edge<Vertex, EdgeDataType> e : edges ){
			if(k.contains(e.v1)){
				result.add(e.v2);
			}
			if(k.contains(e.v2)){
				result.add(e.v1);
			}
		}
		return result;
	}
	
	/**
	 * Treat each vertex by its integer index, rather than the actual vertex's data. 
	 * Make a list of each vertex's parents.
	 * @return
	 */
	public ArrayList<ArrayList<Integer>> parentSets(){
		ArrayList<ArrayList<Integer>> parents = new ArrayList<ArrayList<Integer>>();
		for(Vertex v : verticies){
			parents.add(new ArrayList<Integer>());
		}
		for(Edge<Vertex, EdgeDataType> e : edges){
			parents.get(verticies.indexOf(e.v2)).add(verticies.indexOf(e.v1));
		}
		return parents;
	}
	
	public ArrayList<Vertex> topologicalSort(){
		ArrayList<Integer> resultIndicies = new ArrayList<Integer>();
		boolean[] alreadyProcessed = new boolean[verticies.size()];
		for(int i = 0; i < alreadyProcessed.length; i ++){
			alreadyProcessed[i] = false;
		}
		
		//convert the network into a parents-list format.
		ArrayList<ArrayList<Integer>> parents = parentSets();
		
		// Process all unprocessed verticies.
		// To process a vertex, process all parents, then add it to the list of result indicies.
		
		for(int i =0; i < alreadyProcessed.length ; i ++){
			if(! alreadyProcessed[i]){
				topologicalProcess(resultIndicies, alreadyProcessed, i, parents);
			}
		}
		//Convert the result indicies back into the correct type
		ArrayList<Vertex> result = new ArrayList<Vertex>();
		for(int i : resultIndicies){
			result.add(verticies.get(i));
		}
		return result;
	}
	
	/**
	 * WARNING WARNING - 
	 * may cause an infinite loop if there are cycles.
	 * @param workingList
	 * @param alreadyProcessed
	 * @param vertex
	 * @param parentsList
	 */
	private void topologicalProcess(
			ArrayList<Integer> workingList, 
			boolean[] alreadyProcessed, 
			int vertex,
			ArrayList<ArrayList<Integer>> parentsList){
		for(int parent : parentsList.get(vertex)){
			if(!alreadyProcessed[parent]){
				topologicalProcess(workingList, alreadyProcessed, parent, parentsList);
			}
		}
		workingList.add(vertex);
		alreadyProcessed[vertex] = true;
		return;
	}
	
	
	
	//////////////////////
	//////////////////////
	////////  Testing
	//////////////////////
	//////////////////////
	
	
	public static Network<Integer, Integer> testNetwork(){
			//	0
			//1		2
			//	3
			//
			//	4
			Integer[] testVerticies = new Integer[]{0, 1, 2, 3, 4};
			int[][] testEdges = new int[][]{
					{0, 1},
					{0, 2},
					{1, 3},
					{2, 3},
					{3, 4}
			};
			return new Network<Integer, Integer>(testVerticies, testEdges);	
	}
	
	
	public static void main(String[] args){
		Network<Integer, Integer> testNetwork = testNetwork();
		
		//Test topological sorting
		System.out.println(testNetwork.topologicalSort());
		
	}
	
	
	
}
