import java.sql.*;
import java.util.Properties;

/** 
 * This class handles connections to the database.
 * 
 * @author dannyrivers
 *
 */
public class DatabaseConnector {
	
	static String username = "testUsername";
	static String password = "testPassword";
	static String database = "testDatabase";
	
	
	public static String getPostgresURL( String optionalDatabaseName, String... optionalHostThenPort){
		// See the page https://jdbc.postgresql.org/documentation/head/connect.html
		// in psql java connection documentation.
		String result = "jdbc:postgresql:";
		if(optionalHostThenPort.length < 0){
			if(optionalDatabaseName != null){
				return result + optionalDatabaseName;
			}
			else{
				return result + "/";
			}
		}
		result += "//";
		result += optionalHostThenPort[0];
		if(optionalHostThenPort.length > 1){
			result += ":" + optionalHostThenPort[1];
		}
		result += "/";
		if(optionalDatabaseName != null){
			return result + optionalDatabaseName;
		}
		return result;
	}
	
	public static Connection getConnection() throws SQLException{
		Properties props = new Properties();
		props.setProperty("user", username );
		props.setProperty("password", password);
		return DriverManager.getConnection(getPostgresURL(database), props);
	}	
			
			
}
