import java.sql.Connection;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;


/**
 * This class does the main work of learning a BN skeleton from a distributed database.
 * 
 * Information about the relations in the database (global schema)
 *  is stored in instances of the Relation class.
 *  
 * Each attribute is given a unique Integer identifier based on the relation
 * it comes from, and these integers are associated with the correct names
 * by the attributeNames HashMap.
 * 
 * 
 * @author dannyrivers
 *
 */
public class BabakAlgo {
	
	Connection database;
	Network<Relation, FunctionalDependence> schema;
	
	private double CITolerance = 0.02;
	
	
	
	
	
	public BabakAlgo(Network<Relation, FunctionalDependence> schema, Connection database){
		this.database = database;
		this.schema = schema;
	}
	
	/**
	 * Given a database and a network representing the join strategy, find the bayesian network
	 * efficiently. The network should have the database's relations as nodes, and 
	 * an edge between relations i and j whenever an attribute of i functionally determines
	 * a key of relation j. In other words, if i has a foreign key for j, then i -> j in our network.
	 * The edges of the network should be labeled with the actual functional dependence used,
	 * so if attributes 4 and 6 in relation i functionally determine attributes 2 and 4 in relation j,
	 * (note 2 and 4 must be a key in j), then the edge from i to j will be labeled with the 
	 * functional dependence "[i:4,i:6] -> [j:2,j:4]". To reduce redundancy,
	 * only the local attribute indices are used, so in practice this will be 
	 * 
	 * i ----- FD([4,6], [2,4]) ------> j
	 * 
	 * @param schema
	 * @param database
	 */
	public static void learnBN(){
		
		//TODO remove or clarify this comment section
		//A relation may have a key and/or a foreign key.
		// We need to discover all variables adj to the
		// foreign key in order to associate then with variables in 
		// the smaller relation, and we need to learn a BN on each
		// relation while ignoring the key.
		
		//For each relation, remove its key and learn a BN on the remaining nodes.
		
		// For each relation that has a parent relation in the schema graph,
		// find the set of attributes adj. to the foreign key in that parent relation.
		// (a relation can only have one parent relation, since its parent relation 
		// functionally determines it). If any edge is missing in the child relation,
		// it will be missing after we join the two relations. However, we may need to
		// recheck any edge that is not missing. 
		// 
		
		
		
		//Learn all local BNs within each relation.
		
		
		
		//make a topological sort for the merge order, and merge the BNs that way.
		
		
		
	}
	
	
	
	
	/**
	 * Given a network learned on a number of base relations (workingNetwork),
	 *  where some attribute(s) of r functionally determine every attribute of 
	 *  the working network (these are the foreign keys), and a bayesian
	 *  network learned on r while ignoring keys of r (localNetwork), merge the working network
	 *  and the local network into a new working network. 
	 *  
	 *  Modifies workingNetwork.
	 *   
	 * @param RelationLocalNetwork the network formed by the relation that
	 * functionally determines the other relation.
	 * @param r
	 * @param foreignKeys
	 * @param workingNetwork
	 */
	public void merge(
			Network<Integer, Integer > RelationLocalNetwork, 
			Relation r, 
			HashSet<Integer> foreignKeys,
			Network<Integer, Integer> workingNetwork){
		
		mergeExhaustive(RelationLocalNetwork, r, foreignKeys, workingNetwork);
		
	}
	
	
	/**
	 * Merge the working network into the Local network, following the
	 * specifications of the merge() method, using an exhaustive
	 * method that is very slow.
	 * @param RelationLocalNetwork
	 * @param r
	 * @param foreignKeys
	 * @param workingNetwork
	 */
	public void mergeExhaustive(Network<Integer, Integer > RelationLocalNetwork, 
			Relation r, 
			HashSet<Integer> foreignKeys,
			Network<Integer, Integer> workingNetwork){
		
		for(Integer attribute: r.attributes){
			workingNetwork.addVertex(attribute);
		}
		
		
		
		//First, find the context set of things we might need to condition on.
		// Smaller context sets are important, because the next steps are exponential
		// in the size of contextSet.
		Integer[] originalVerticiesInWorkingNetwork = workingNetwork.verticies.toArray(
				new Integer[workingNetwork.verticies.size()]);
		HashSet<Integer> contextSet = new HashSet<Integer>(workingNetwork.verticies);
		contextSet.removeAll(r.primaryKey);

		
		//Test all edges in the workingNetwork that might be blocked by the new variables
		// in r, and see if any need to be removed.
		ArrayList<Edge<Integer, Integer>> toRemove = new ArrayList<Edge<Integer, Integer>>();
		for(Edge<Integer, Integer> e : workingNetwork.edges){
			if(testExhaustiveIndep(e.v1, e.v2, contextSet)){
				toRemove.add(e);
			}
		}
		workingNetwork.edges.removeAll(toRemove);

		//Test all edges from the original workingNetwork to neighbors of the foreign keys in r.
		//   those attributes in r may now be adjacent to an attribute in the working network, 
		//   since we will remove the keys from the final network.
		for(Integer v1 : RelationLocalNetwork.neighbors(foreignKeys)){
			for(Integer v2 : originalVerticiesInWorkingNetwork){
				//We want to see if v1 and v2 might be adjacent - in other words, remove
				// the edge between v1 and v2 iff there is some set z where (v1 || v2 | z). 
				// This ensures that WorkingNetwork is always an I-map.
				if(!testExhaustiveIndep(v1, v2, contextSet)){
					workingNetwork.addEdge(new Edge<Integer, Integer>(v1, v2));
				}
			}
		}
		
		
		
		//We don't need to test edges in r's local network, to see if they
		// need to be removed, they never will. This is because the foreign keys from
		// r determine workingNetwork. The latter fact means that, 
		// if two attributes in r were always
		// dependent, conditional on any set of variables in r
		// (i.e., they are adjacent in the local network), then these two attributes
		// will always be dependent conditional on any set of variables in r and 
		// workingNetwork, since all attributes of workingNetwork are functionally determined by
		// the foreign keys and the dependence between these two attributes was not
		// destroyed by the foreign keys.
		
		//No new edges can be added between variables in localNetwork or variables
		// both originally in workingNewtork. In both cases, there is some context set
		// that makes our two variables independent - we can always recover a new context set
		// that also makes them independent
		// (either using the exact same context set, or if the old one used foreign keys, 
		// using some of the variables in r as part of the context set.)
		
	}
	
	/**
	 * Merge the working network into the Local network, following the
	 * specifications of the merge() method, using the grow shrink algorithm.
	 * @param RelationLocalNetwork
	 * @param r
	 * @param foreignKeys
	 * @param workingNetwork
	 */
	public void mergeGS(
			Network<Integer, Integer > RelationLocalNetwork, 
			Relation r, 
			HashSet<Integer> foreignKeys,
			Network<Integer, Integer> workingNetwork){
		
		//workingNetwork will include the key k for r. (k may be a set of attributes)
		// We want to consider each edge between each neighbor N(k) and each attribute of r.
		// We can use the grow-shrink algorithm to find the markov boundary of each node under consideration,
		// TODO IS THIS VALID?
		// 
	}

	
	
	
	
	/**
	 * Check if x and y are ever rendered independent by 
	 * any subset of contextSet.
	 * @param x
	 * @param y
	 * @param contextSet
	 */
	public boolean testExhaustiveIndep(int x, int y, HashSet<Integer> contextSet){
		for(ArrayList<Integer> possibleContext : new CombinationsIterator<Integer>(contextSet)){
			if(mutualInformation(x, y, possibleContext) < CITolerance){
				return true;
			}
		}
		return false;
	}
	
	
	
	
	/**
	 * Find the mutual information between x and y conditional on
	 * all attributes in the context.
	 * 
	 * According to Wikipedia (11/24/2017) 
	 *   https://en.wikipedia.org/wiki/Mutual_information
	 * the mutual information between two variables is calculated by the formula
	 * sum_x   sum_y   p(x, y)  log ( p(x,y) / (p(x)*p(y) ) )
	 * 
	 * Using counts rather than probabilities, this can be approximated by a sql query.
	 * 
	 * We can group by (context, x, y) and find sum(frequency). We can group that by 
	 * (y) to find p(x), or by (x) to find p(y)
	 * @param x
	 * @param y
	 * @param context
	 */
	public double mutualInformation(int x, int y, Iterable<Integer> context){
		
		
		String query = "SELECT " + contextAttributes + "," + xAttribute + "," + yAttribute + "COUNT(*) \n"
		"FROM " relations;
	}
	
	
	
	
	
	

	
	
	
	
	

	////////
	////////
	//////Comments saved for future reference.
	////////
	////////
	
	
	///Writing the mergeExhaustive method - Do we need to condition on things in 
	/// the determining relation that aren't adjacent to foreign key nodes 
	/// when trying to test separation of a pair v1 in workingNetwork, v2 in localNetwork?
	//Proof that we don't need to include every single vertex in r in our
	// possible sets z:
	//Say there is some vertex l1 in the local network that isn't adjacent to any
	// of the foreign key verticies. Then l1 is non-adjacent to every node in
	// the working network, since each node in the working network
	// is a function of the foreign keys, and (x || y | z) ==> ( f(x) || y | z). 
	// This uses the fact that relationLocalNetwork is an I-map, which is guaranteed.
	
	// This means, if some context set C union l1
	// separates v1 and v2, (see below for def of v1 and v2) we have:
	// (v1 || v2 | C U l1), (v2 || l1 | D), 
}
