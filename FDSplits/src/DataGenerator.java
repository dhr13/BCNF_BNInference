import java.sql.Connection;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Random;


/**
 * This class uses a DiscreteBayesianNetwork to generate data and insert
 * it into a database.
 * @author dannyrivers
 *
 */
public class DataGenerator {
	static Random r = new Random();
	
	public static void generateData(int numTuples, DiscreteBayesianNetwork d ){
		int numVariables = d.cpts.length;
		//We want to pick random sets of the variables and create an ID for them.
		// For example, if our variables are 1, 2, 3, 4, 5, 6, 7
		// then we might take 1, 2, 3 and create key "123", then take 4, 7, "123" and create key
		// "47123", and be left with a final schema "47123", 5, 6.
		// Then each tuple should be decomposed into 3 parts:
		// 1:val1, 1:val2, 3:val3, 123:val123
		// 4:val4, 7:val7, 123:val123, 47123:val47123
		// 47123:val47123, 5:val5, 6:val6
		//and stored in relations accordingly.
		
		
		//The process for deciding splits will be as follows:
		// Pick a number of keys (IDs) to make.
		// For each key, pick a number of variables to determine out of the list of all variables
		// left (including the new IDs, but excluding the things determined by the IDs).
		
		ArrayList<Object> attributes = new ArrayList<Object>();
		for(int i = 0; i < numVariables; i ++){
			attributes.add(i);
		}
		ArrayList<ArrayList<Object>> schemas = new ArrayList<ArrayList<Object>>();
		int numKeys = 2 ;//r.nextInt(numVariables / 3); //dividing by 3
		// ensures that we never try to make more keys than possible.
		for(int i = 0 ; i < numKeys ; i ++){
			int numVariablesDetermined = r.nextInt(4);
			ArrayList<Object> determined = new ArrayList<Object>();
			for(int j = 0; j < numVariablesDetermined; j ++){
				int nextAttribute = r.nextInt(attributes.size());
				determined.add(attributes.get(nextAttribute));
				attributes.remove(nextAttribute);
			}
			attributes.add(keyFrom(determined));
			determined.add(keyFrom(determined));
			schemas.add(determined);
		}
		schemas.add(attributes);
		
		
		
		
		
		// Now we have the schema all built.
		// For any tuple, we can pull out the values we want to insert into the different tables
		// (each ID will be string-valued).
		
		Connection c = null;
		try{
			//c = DatabaseConnector.getConnection();
			int batchSize = 100;
			int i = 0;
			for(; i < numTuples - batchSize ; i += batchSize){
				//Statement smt = c.createStatement();
				for(int j = 0; j < batchSize; j ++){
					//Make the tuple
					Integer[] tuple = d.feedForward();
					//Split the tuple into the correct smaller tuples, and insert the smaller tuples
					//into the database.
					for(ArrayList<Object> schema : schemas){
						Object[] values = new Object[schema.size()];
						int valueIndex = 0;
						for(Object o : schema){
							if(o instanceof String){
								String[] parts = ((String) o).replaceAll("[()]", "").split(",");
								int[] subValues = new int[parts.length];
								for(int k = 0; k < subValues.length ; k ++){
									subValues[k] = tuple[Integer.parseInt(parts[k])];
								}
								values[valueIndex] = Arrays.toString(subValues);
							}
							else{
								values[valueIndex] = tuple[((int)o)];
							}
							valueIndex ++;
						}
						String tableName = "" + schema;
						System.out.println( tableName + "\t:" + Arrays.toString(values));
						/* TODO
						String statement = "INSERT INTO " + tableName + " VALUES ( )";
						smt.executeUpdate(statement);
						*/
					}
				}
			}
			//last batch
			while(i < numTuples){
				System.out.println("Unimplemented: Skipping" + i);
				i++;
			}
			
			
		} catch(Exception e){e.printStackTrace();}
		/*
		catch(SQLException s){
			System.out.println("Could not connect to the database:");
			s.printStackTrace();
		}
		finally{
			if(c != null){
				try {
					c.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}*/
		
	}
	
	
	
	
	
	private static String keyFrom(ArrayList<Object> t){
		String result = "(";
		//first, all subkeys.
		// Then, anything not contained in a subkey.
		HashSet<Integer> banned = new HashSet<Integer>();
		for(Object o : t){
			if(o instanceof String){
				// this is a key of the form (1, 2, 3).
				// Add it to the result, and make sure not to include 1, 2, or 3.
				String tString=  (String)o;
				if(tString.contains("(")){
					result += tString + ",";
					//don't use anything already specified by this key.
					for(String i : tString.replaceAll("[()]", "").split(",")){
						banned.add(Integer.parseInt(i));
					}
				}
			}
		}
		for(Object o : t){
			if(o instanceof Integer){
				if(!banned.contains((int)o)){
					result += o + ",";
				}
			}
		}
		return result.substring(0, result.length() - 1) + ")"; // cut off the last
		// comma and add a closing parens
	}
	
	public static void main(String[] args){
		generateData(1000, new DiscreteBayesianNetwork(Network.testNetwork(), 3));
	}
}
