
/**
 * This class handles conversions bewteen attributes (strings)
 * and their unique identifiers (formed based on the relation number and attribute number).
 * 
 * It allows attribute locations to be added to hash maps and hash sets.
 * 
 *
 * @author dannyrivers
 *
 */
public class AttributeLocation {
	int relationNumber;
	int attributeNumber;
	
	public AttributeLocation(int first, int second){
		this.relationNumber = first;
		this.attributeNumber = second;
	}

	public int getRelNum() {
		return relationNumber;
	}

	public int getAttrNum() {
		return attributeNumber;
	}
	
	
	
	@Override
	public boolean equals(Object o){
		if(! (o instanceof AttributeLocation)){
			return false;
		}
		AttributeLocation p = (AttributeLocation)o;
		return (this.relationNumber == p.relationNumber) && (this.attributeNumber == p.attributeNumber);
	}
	
	@Override 
	public int hashCode(){
		return relationNumber << 10  + attributeNumber;
		//Most relations have very few attributes, the left shift of
		//relationNumber helps prevent collisions.
	}
	
}
